﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AudioLabelerTool
{
    internal static class Program
    {

        public static string initialOpeningFile = null;
        public static bool justExportAndExit = false;

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if(args.Length > 0)
            {
                string maybepath = args[0].Trim('"');
                maybepath = Path.Combine(Path.GetDirectoryName(maybepath), Path.GetFileNameWithoutExtension(maybepath)); 
                if (File.Exists(maybepath)) { 
                    initialOpeningFile = maybepath;
                    if(args.Length > 1 && args[1] == "/export")
                    {
                        justExportAndExit = true;
                    }
                }
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }



        
    }

    public class AudioLabel
    {
        
        public string id { get; set; } = Guid.NewGuid().ToString();
        public float Begin { get; set; }
        public float End { get; set; }

        public string Transcription { get; set; }
    }


}

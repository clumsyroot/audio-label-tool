﻿using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioLabelerTool
{
    internal class AudioPlayer : IDisposable
    {

        private AudioFileReader audioFileReader;
        private WasapiOut waveOut;
        public AudioPlayer(string audioPath) {
            audioFileReader = new AudioFileReader(audioPath);
            waveOut = new WasapiOut(NAudio.CoreAudioApi.AudioClientShareMode.Shared,32);
            
            waveOut.Init(audioFileReader);
            
        }

        public bool IsPlaying { 
            get
            {
                return waveOut.PlaybackState == PlaybackState.Playing;
            }
            set
            {
                if (value)
                {
                    waveOut.Play();
                }
                else
                {
                    waveOut.Pause();
                    
                }
            }
        }

        public float TotalSecond
        {
            get
            {
                return (float)audioFileReader.TotalTime.TotalSeconds;
            }
        }

        public float Position
        {
            get
            {
                return (float)audioFileReader.CurrentTime.TotalSeconds;

            }
            set
            {
                audioFileReader.CurrentTime = TimeSpan.FromSeconds(value);
            }
        }


        public void Dispose()
        {
            waveOut?.Dispose();
            audioFileReader?.Dispose();
        }
    }
}

﻿namespace EssentialDialog
{
    partial class FrmLongLoading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.animTimer = new System.Windows.Forms.Timer(this.components);
            this.tblProgressContainer = new System.Windows.Forms.TableLayoutPanel();
            this.lblMsg = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel114514 = new System.Windows.Forms.Panel();
            this.tblProgressBar = new System.Windows.Forms.Panel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tblProgressContainer.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel114514.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(600, 2);
            this.label1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Location = new System.Drawing.Point(0, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(600, 2);
            this.label2.TabIndex = 1;
            // 
            // animTimer
            // 
            this.animTimer.Enabled = true;
            this.animTimer.Interval = 1;
            this.animTimer.Tick += new System.EventHandler(this.animTimer_Tick);
            // 
            // tblProgressContainer
            // 
            this.tblProgressContainer.ColumnCount = 3;
            this.tblProgressContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.00331F));
            this.tblProgressContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tblProgressContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.99669F));
            this.tblProgressContainer.Controls.Add(this.lblMsg, 0, 0);
            this.tblProgressContainer.Controls.Add(this.panel1, 1, 1);
            this.tblProgressContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblProgressContainer.Location = new System.Drawing.Point(0, 6);
            this.tblProgressContainer.Name = "tblProgressContainer";
            this.tblProgressContainer.RowCount = 2;
            this.tblProgressContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.05479F));
            this.tblProgressContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.94521F));
            this.tblProgressContainer.Size = new System.Drawing.Size(600, 148);
            this.tblProgressContainer.TabIndex = 2;
            // 
            // lblMsg
            // 
            this.tblProgressContainer.SetColumnSpan(this.lblMsg, 3);
            this.lblMsg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMsg.Font = new System.Drawing.Font("微软雅黑", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblMsg.ForeColor = System.Drawing.Color.White;
            this.lblMsg.Location = new System.Drawing.Point(3, 0);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Padding = new System.Windows.Forms.Padding(0, 0, 0, 6);
            this.lblMsg.Size = new System.Drawing.Size(594, 77);
            this.lblMsg.TabIndex = 1;
            this.lblMsg.Text = "请稍后...";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.panel114514);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(103, 80);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(2);
            this.panel1.Size = new System.Drawing.Size(394, 25);
            this.panel1.TabIndex = 2;
            // 
            // panel114514
            // 
            this.panel114514.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel114514.Controls.Add(this.tblProgressBar);
            this.panel114514.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel114514.Location = new System.Drawing.Point(2, 2);
            this.panel114514.Name = "panel114514";
            this.panel114514.Padding = new System.Windows.Forms.Padding(2);
            this.panel114514.Size = new System.Drawing.Size(390, 21);
            this.panel114514.TabIndex = 0;
            // 
            // tblProgressBar
            // 
            this.tblProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblProgressBar.Location = new System.Drawing.Point(2, 2);
            this.tblProgressBar.Name = "tblProgressBar";
            this.tblProgressBar.Size = new System.Drawing.Size(386, 17);
            this.tblProgressBar.TabIndex = 0;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // FrmLongLoading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(600, 160);
            this.Controls.Add(this.tblProgressContainer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmLongLoading";
            this.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            this.Text = "FrmLongLoading";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmLongLoading_FormClosed);
            this.Load += new System.EventHandler(this.FrmLongLoading_Load);
            this.tblProgressContainer.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel114514.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer animTimer;
        private System.Windows.Forms.TableLayoutPanel tblProgressContainer;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel114514;
        private System.Windows.Forms.Panel tblProgressBar;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}